deps:
	git submodule init;
	go mod tidy;

build-consumer: deps
	go build -o bin/consumer cmd/consumer/main.go

build-publisher: deps
	go build -o bin/publisher cmd/publisher/main.go

dc-start-dev: build
	docker-compose \
		-f deployment/docker/rmq.docker-compose.yaml \
		-f deployment/docker/services.docker-compose.yaml \
		--env-file=deployment/docker/.env.dev \
		up --build --force-recreate -d

dc-down-dev:
	docker-compose \
		-f deployment/docker/rmq.docker-compose.yaml \
		-f deployment/docker/services.docker-compose.yaml \
	  	--env-file=deployment/docker/.env.dev \
		down
