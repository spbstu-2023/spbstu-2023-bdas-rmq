# САБД - Kur work
## О задании
Name: Создание RMQ очереди для передачи данных с обсускацией

Author: Andrianov Artemii

Group: #5140904/30202

## Цели
- Подготовить примеры `consumer` и `producer` запускаемых в Docker c `rabbitmq`
- Подготовить доклад про использование RMQ

## Prerequisite
- [Go 1.21](https://tip.golang.org/doc/go1.21)
- [Docker](https://www.docker.com/)

## Запуск работ
### Запуск docker-compose
Для запуска в виде docker-compose использовать:
```bash 
    make dc-start-dev
```

### Остановка docker-compose
Для остановки docker-compose использовать:
```bash 
    make dc-down-dev
```
