package connector

import (
	"fmt"
	"github.com/wagslane/go-rabbitmq"
)

func NewRabbitConn(config *Config) (*rabbitmq.Conn, error) {
	connStr := fmt.Sprintf("amqp://%s:%s@%s", config.Username, config.Password, config.Host)
	conn, err := rabbitmq.NewConn(connStr)
	if err != nil {
		return nil, err
	}
	return conn, nil
}
