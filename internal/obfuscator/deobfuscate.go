package obfuscator

import "reflect"

func DeobfuscateFields(obj interface{}, shift int) {
	objType := reflect.TypeOf(obj).Elem()
	objValue := reflect.ValueOf(obj).Elem()

	for i := 0; i < objType.NumField(); i++ {
		field := objValue.Field(i)
		if field.CanInterface() {
			switch field.Kind() {
			case reflect.String:
				field.SetString(deobfuscateField(field.String(), shift))
			}
		}
	}
}

func deobfuscateField(obfuscated string, shift int) string {
	data := make([]byte, len(obfuscated))
	for i, b := range []byte(obfuscated) {
		data[i] = b - byte(shift)
	}
	return string(data)
}
