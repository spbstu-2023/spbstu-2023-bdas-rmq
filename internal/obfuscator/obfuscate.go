package obfuscator

import "reflect"

func ObfuscateFields(obj interface{}, shift int) {
	objType := reflect.TypeOf(obj).Elem()
	objValue := reflect.ValueOf(obj).Elem()

	for i := 0; i < objType.NumField(); i++ {
		field := objValue.Field(i)
		if field.CanInterface() {
			switch field.Kind() {
			case reflect.String:
				field.SetString(obfuscateField(field.String(), shift))
			}
		}
	}
}

func obfuscateField(data string, shift int) string {
	obfuscated := make([]byte, len(data))
	for i, b := range []byte(data) {
		obfuscated[i] = b + byte(shift)
	}
	return string(obfuscated)
}
