package domain

import "time"

type Message struct {
	Timestamp time.Time `json:"timestamp"`
	Author    string    `json:"author"`
	Data      string    `json:"data"`
}
