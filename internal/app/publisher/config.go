package publisher

import "gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/connector"

type Config struct {
	ConnConfig      *connector.Config `yaml:"conn-config"`
	PublisherConfig *PublisherConfig  `yaml:"publisher-config"`
}

type PublisherConfig struct {
	ExchangeName string   `yaml:"exchange-name"`
	RoutingKeys  []string `yaml:"routing-keys"`
	ExchangeKing string   `yaml:"exchange-king"`
}
