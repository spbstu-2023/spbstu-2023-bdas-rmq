package publisher

import (
	"context"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"github.com/wagslane/go-rabbitmq"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/connector"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/domain"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/obfuscator"
)

type App struct {
	conn      *rabbitmq.Conn
	config    *PublisherConfig
	publisher *rabbitmq.Publisher
}

func NewApp(config *Config) (*App, error) {
	conn, err := connector.NewRabbitConn(config.ConnConfig)
	if err != nil {
		return nil, err
	}

	publisher, err := newRabbitPublisher(conn, config.PublisherConfig)
	if err != nil {
		log.Errorf("error while create publisher: %w", err)
		return nil, err
	}

	return &App{
		conn:      conn,
		publisher: publisher,
		config:    config.PublisherConfig,
	}, nil
}

func newRabbitPublisher(conn *rabbitmq.Conn, config *PublisherConfig) (*rabbitmq.Publisher, error) {
	publisher, err := rabbitmq.NewPublisher(
		conn,
		rabbitmq.WithPublisherOptionsLogging,
		rabbitmq.WithPublisherOptionsExchangeName(config.ExchangeName),
		rabbitmq.WithPublisherOptionsExchangeKind(config.ExchangeKing),
		rabbitmq.WithPublisherOptionsExchangeDeclare,
	)
	return publisher, err
}

func (a *App) Run(ctx context.Context, message *domain.Message) error {
	log.Infof("Source message: %v", message)
	obfuscator.ObfuscateFields(message, 5)
	log.Infof("Obfuscated message: %v", message)
	data, err := json.Marshal(message)

	if err != nil {
		log.Errorf("error while create conn: %w", err)
		return err
	}

	err = a.publisher.PublishWithContext(
		ctx,
		data,
		a.config.RoutingKeys,
		rabbitmq.WithPublishOptionsContentType("application/json"),
		rabbitmq.WithPublishOptionsMandatory,
		rabbitmq.WithPublishOptionsPersistentDelivery,
		rabbitmq.WithPublishOptionsExchange(a.config.ExchangeName),
	)

	if err != nil {
		return err
	}
	return nil
}

func (a *App) Close() error {
	a.publisher.Close()
	if err := a.conn.Close(); err != nil {
		return err
	}
	return nil
}
