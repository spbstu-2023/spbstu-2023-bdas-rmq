package consumer

import (
	"bytes"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"github.com/wagslane/go-rabbitmq"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/connector"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/domain"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/obfuscator"
	"math/rand"
	"time"
)

type App struct {
	conn     *rabbitmq.Conn
	consumer *rabbitmq.Consumer
}

func NewAppAndRun(config *Config) (*App, error) {
	conn, err := connector.NewRabbitConn(config.ConnConfig)
	if err != nil {
		log.Errorf("error while create conn: %w", err)
		return nil, err
	}

	consumer, err := newRabbitConsumer(conn, config.ConsumerConfig, handlerFunc)
	if err != nil {
		log.Errorf("error while work with consumer: %w", err)
		return nil, err
	}

	return &App{
		conn:     conn,
		consumer: consumer,
	}, nil
}

func newRabbitConsumer(conn *rabbitmq.Conn, config *ConsumerConfig, handler rabbitmq.Handler) (*rabbitmq.Consumer, error) {
	consumer, err := rabbitmq.NewConsumer(
		conn,
		handler,
		config.QueueName,
		rabbitmq.WithConsumerOptionsRoutingKey(config.RoutingKey),
		rabbitmq.WithConsumerOptionsExchangeName(config.ExchangeName),
		rabbitmq.WithConsumerOptionsQOSPrefetch(config.PrefetchConfig),
		rabbitmq.WithConsumerOptionsExchangeDeclare,
	)
	return consumer, err
}

const (
	NackRequeueData = "REQUEUE DATA"
)

func handlerFunc(d rabbitmq.Delivery) (action rabbitmq.Action) {
	time.Sleep(time.Duration(rand.Intn(3)) * time.Second)
	message := &domain.Message{}
	if err := json.NewDecoder(bytes.NewBuffer(d.Body)).Decode(&message); err != nil {
		return rabbitmq.NackDiscard
	}
	log.Infof("Obfuscated message: %v", message)
	obfuscator.DeobfuscateFields(message, 5)
	log.Infof("Source message: %v", message)
	if message.Data == NackRequeueData {
		if rand.Intn(2) == 0 {
			return rabbitmq.NackRequeue
		}
	}
	log.Infof("get message at %v by %s: %s", message.Timestamp, message.Author, message.Data)
	return rabbitmq.Ack
}

func (a *App) Close() error {
	a.consumer.Close()
	if err := a.conn.Close(); err != nil {
		return err
	}
	return nil
}
