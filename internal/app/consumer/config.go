package consumer

import "gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/connector"

type Config struct {
	ConnConfig     *connector.Config `yaml:"conn-config"`
	ConsumerConfig *ConsumerConfig   `yaml:"consumer-config"`
}

type ConsumerConfig struct {
	QueueName      string `yaml:"queue-name"`
	ExchangeName   string `yaml:"exchange-name"`
	RoutingKey     string `yaml:"routing-key"`
	PrefetchConfig int    `yaml:"prefetch-config"`
}
