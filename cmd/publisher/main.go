package main

import (
	"context"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/app/consumer"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/app/publisher"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/domain"
	"gopkg.in/yaml.v3"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

var pathToFile = flag.String("config", "configs/config-publisher.yaml", "Path to YAML config")

func main() {
	flag.Parse()

	file, err := os.Open(*pathToFile)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	config := &publisher.Config{}
	err = yaml.NewDecoder(file).Decode(&config)
	if err != nil {
		log.Panic(err)
	}

	app, err := publisher.NewApp(config)
	if err != nil {
		log.Panic(err)
	}
	defer app.Close()

	ctx, close := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer close()
	for i := 0; i < 100; i++ {
		message := fmt.Sprintf("request #%d", i)
		if rand.Intn(5) == 0 {
			message = consumer.NackRequeueData
		}
		if err := app.Run(ctx, &domain.Message{
			Timestamp: time.Now(),
			Data:      message,
			Author:    "publisher",
		}); err != nil {
			log.Panic(err)
		}
		log.Printf("Message %s send into rmq at %s", message, time.Now())
		time.Sleep(time.Duration(rand.Intn(3)) * time.Second)
	}

}
