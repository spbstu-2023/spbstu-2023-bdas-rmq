package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq/internal/app/consumer"
	"gopkg.in/yaml.v3"
	"os"
	"os/signal"
)

var pathToFile = flag.String("config", "configs/config-consumer.yaml", "Path to YAML config")

func main() {
	flag.Parse()

	file, err := os.Open(*pathToFile)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	config := &consumer.Config{}
	err = yaml.NewDecoder(file).Decode(&config)
	if err != nil {
		log.Panic(err)
	}

	app, err := consumer.NewAppAndRun(config)
	if err != nil {
		log.Panic(err)
	}
	defer app.Close()

	sign := make(chan os.Signal)
	signal.Notify(sign, os.Interrupt, os.Kill)

	<-sign
}
