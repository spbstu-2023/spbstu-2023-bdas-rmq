module gitlab.com/spbstu-2023/spbstu-2023-bdas-rmq

go 1.21.4

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/wagslane/go-rabbitmq v0.12.4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/rabbitmq/amqp091-go v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
